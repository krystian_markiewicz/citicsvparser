﻿using System;
using CitiCsvParser.Bll.FileImports;
using CitiCsvParser.Bll.Services.Interfaces;

namespace CitiCsvParser.Bll.Services
{
    public sealed class CsvImportService : ICsvImportService
    {
        private readonly CsvImport _import;

        public CsvImportService(string path, string filterColumn, string filterValue)
        {
            _import = new CsvImport(path, filterColumn, filterValue);
        }

        public void ParseFile(char separator = ',')
        {
            if (this.CheckErrors())
            {
                _import.ParseFile(separator);
            }
        }

        public void ShowResult()
        {
            if (this.CheckErrors())
            {
                _import.ShowResult();
            }
            else
            {
                this.ShowErrors();
            }
        }

        public string GetErrors()
        {
            return _import.GetErrors();
        }

        private bool CheckErrors()
        {
            return String.IsNullOrEmpty(_import.GetErrors());
        }

        private void ShowErrors()
        {
            Console.WriteLine(_import.GetErrors());
        }
    }
}
