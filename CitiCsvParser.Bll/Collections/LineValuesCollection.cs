﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CitiCsvParser.Bll.Collections
{
    public class LineValuesCollection
    {
        public LineValuesCollection()
        {
            LineValues = new List<List<string>>();
        }

        public List<List<string>> LineValues { get; private set; }

        public IEnumerator GetEnumerator()
        {
            return LineValues.GetEnumerator();
        }

        public void AddValues(IEnumerable<string> values)
        {
            LineValues.Add(values.ToList());
        }

        public bool AddValueToLine(int index, string value)
        {
            if (index <= LineValues.Count - 1)
            {
                LineValues[index].Add(value);
                return true;
            }

            return false;
        }
    }
}
