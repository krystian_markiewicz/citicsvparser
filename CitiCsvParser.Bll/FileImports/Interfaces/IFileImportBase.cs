﻿namespace CitiCsvParser.Bll.FileImports.Interfaces
{
    public interface IFileImportBase
    {
        void ParseFile(char separator = ',');

        void ShowResult();

        string GetErrors();
    }
}
