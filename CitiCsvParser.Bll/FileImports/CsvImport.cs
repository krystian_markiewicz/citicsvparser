﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CitiCsvParser.Bll.FileImports
{
    public sealed class CsvImport : FileImportBase
    {
        #region Fields

        private readonly List<string> _values = new List<string>(); 

        private readonly StringBuilder _stringBuilder = new StringBuilder();

        private bool _doubleQuotes = false;

        private bool _ignoreDoubleQuotes = false;

        private bool _isContinue = false;

        // Optional task - align columns
        private readonly List<int> _charNumbers = new List<int>();
        // ---

        // Optional task - filter columns
        private readonly string _filterColumn;

        private readonly string _filterValue;

        private bool _firstRow = true;

        private int _columnNumber = -1;
        // ---

        #endregion

        #region Constructors

        public CsvImport(string path, string filterColumn, string filterValue) : base(path)
        {
            _filterColumn = filterColumn;
            _filterValue = filterValue;
        }

        #endregion

        public override void ParseFile(char separator = ',')
        {
            foreach (var line in FileLines)
            {
                this.ParseFileLine(line, separator);
                this.EndParsingFullLine();
            }

            this.AlignColumns();
        }

        public override void ShowResult()
        {
            foreach (var line in ParsedFileData.LineValues)
            {
                Console.WriteLine("|{0}|", String.Join("|", line));
            }
        }

        // Optional task - column alignment
        private void AlignColumns()
        {
            foreach (var line in this.ParsedFileData.LineValues)
            {
                for (int index = 0; index < line.Count; index++)
                {
                    ChangeColumnWidth(index, line);
                }
            }
        }

        private void ChangeColumnWidth(int index, List<string> line)
        {
            if (_charNumbers[index] > line[index].Length)
            {
                var length = _charNumbers[index] - line[index].Length;
                var stringBuilder = new StringBuilder(line[index]);
                for (int i = 0; i < length; i++)
                {
                    stringBuilder.Append(" ");
                }

                line[index] = stringBuilder.ToString();
            }
        }
        // ---

        private void ParseFileLine(string line, char separator)
        {
            for (int index = 0; index < line.Length; index++)
            {
                var sign = line[index];

                this.CheckIfIgnoreNextDoubleQoutes();
                this.CheckIfIsFirstDoubleQuotes(sign);
                this.CheckIfIsNextDoubleQoutes(sign, line, index);
                this.CheckIfIsSeparator(sign, separator);
                this.CheckIfAppendToStringBuilder(sign);
            }
        }

        private void EndParsingFullLine()
        {
            if (!_doubleQuotes)
            {
                _values.Add(_stringBuilder.ToString());
                if (this.ParsedFileData.LineValues.Any())
                {
                    CheckCorrectColumnNumber();
                }

                if (this.FilterColumns() || _firstRow)
                {
                    this.ColumnCharNumber(_stringBuilder.Length);
                    this.ParsedFileData.AddValues(_values);
                }
                
                _values.Clear();
                _stringBuilder.Clear();
            }
        }

        // Optional task - filter rows
        private bool FilterColumns()
        {
            for (int i = 0; i < _values.Count; i++)
            {
                if (_firstRow && _values[i].Equals(_filterColumn))
                {
                    _columnNumber = i;
                    _firstRow = false;
                    return true;
                }
                
                if (_columnNumber == i && _values[i].Contains(_filterValue))
                {
                    return true;
                }
            }

            return false;
        }
        // ---

        private void CheckCorrectColumnNumber()
        {
            var lastIndex = this.ParsedFileData.LineValues.Count - 1;
            var lastRowColumnNumber = this.ParsedFileData.LineValues[lastIndex].Count;
            if (lastRowColumnNumber < _values.Count)
            {
                Errors += String.Format("Too many columns in {0} row\n\r", lastIndex + 2);
            }

            if (lastRowColumnNumber > _values.Count)
            {
                Errors += String.Format("Too few columns in {0} row\n\r", lastIndex + 2);
            }
        }

        private void CheckIfIgnoreNextDoubleQoutes()
        {
            var condition = this.CheckIsContinue(_ignoreDoubleQuotes);
            if (condition)
            {
                _ignoreDoubleQuotes = false;
                _isContinue = true;
            }
        }

        private void CheckIfIsFirstDoubleQuotes(char sign)
        {
            var condition = this.CheckIsContinue(this.AreEqual('"', sign) && !_doubleQuotes);
            if (condition)
            {
                _doubleQuotes = true;
                _isContinue = true;
            }
        }

        private void CheckIfIsNextDoubleQoutes(char sign, string line, int index)
        {
            var condition = this.CheckIsContinue(this.AreEqual('"', sign) && _doubleQuotes);
            if (condition)
            {
                if (line.Length > index + 1 && this.AreEqual('"', line[index + 1]))
                {
                    _ignoreDoubleQuotes = true;
                }
                else
                {
                    _doubleQuotes = false;
                    _isContinue = true;
                }
            }
        }

        private void CheckIfIsSeparator(char sign, char separator)
        {
            var condition = this.CheckIsContinue(this.AreEqual(separator, sign) && !_doubleQuotes);
            if (condition)
            {
                _values.Add(_stringBuilder.ToString());
                this.ColumnCharNumber(_stringBuilder.Length);

                _stringBuilder.Clear();
                _isContinue = true;
            }
        }

        private void ColumnCharNumber(int charNumber)
        {
            if (_charNumbers.Count >= _values.Count)
            {
                if (_charNumbers[_values.Count - 1] < charNumber)
                {
                    _charNumbers[_values.Count - 1] = charNumber;
                }

                return;
            }

            _charNumbers.Add(charNumber);
        }

        private void CheckIfAppendToStringBuilder(char sign)
        {
            var condition = this.CheckIsContinue();
            if (condition)
            {
                _stringBuilder.Append(sign);
            }
            else
            {
                _isContinue = false;
            }
        }

        private bool CheckIsContinue(bool condition = true)
        {
            return !_isContinue && condition;
        }
    }
}
