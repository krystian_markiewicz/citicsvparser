﻿using System;
using System.Collections.Generic;
using System.IO;
using CitiCsvParser.Bll.Collections;
using CitiCsvParser.Bll.FileImports.Interfaces;

namespace CitiCsvParser.Bll.FileImports
{
    public abstract class FileImportBase : IFileImportBase
    {
        protected IEnumerable<string> FileLines;

        protected LineValuesCollection ParsedFileData;

        protected string Errors;

        protected FileImportBase(string path)
        {
            FileLines = new List<string>();
            ParsedFileData = new LineValuesCollection();
            Errors = this.CheckIfFilePathExists(path);
            if (String.IsNullOrEmpty(Errors))
            {
                this.ReadFileLines(path);
            }
        }

        public abstract void ParseFile(char separator = ',');

        public abstract void ShowResult();

        public string GetErrors()
        {
            return Errors;
        }

        protected bool AreEqual(char first, char second)
        {
            return first.Equals(second);
        }

        private string CheckIfFilePathExists(string path)
        {
            if (File.Exists(path))
            {
                return String.Empty;
            }

            return String.Format("File \"{0}\" does not exist\n\r", path);
        }

        private void ReadFileLines(string path)
        {
            FileLines = File.ReadLines(path);
        }
    }
}
