﻿using CitiCsvParser.App.Dependencies;
using CitiCsvParser.Bll.Services.Interfaces;

// To show whole process of creating this application, I pushed each step as a single commit.
// Firstly I created a minimum value product, a simple application that did realize a task,
// but only for most standard CSV file. In each next step I added additional functionality
// and made refactoring if necessary.
//
// As for design, I have created the base abstract class for file import to ansure extensibility
// for other file imports' types. Access to each file import, here for the CSV import, is done by
// service class, which is a proxy that creates CsvImport object and handles its functionalities.
// I have created a common IFileImportBase interface so that each import class and its service class
// will have the same methods. This would help also if there was a need to use reflection or
// generic types for this process. For dependency container, I used CastleWindsor. This way dependency
// injection for service classes' interfaces can be put in one place where implementation can
// be chosen. I have also created a collection wrapper for a list that stores parsed file data.
// Such collection is easier than using List<List<string>> and can have addition functionalities that are
// specific for it. I have also create a class to handle program input arguments, because client should not
// worry or see how they are mapped, just use them in most comfortable way.
//
// Of course whenever it was possible I tried to use many good programming practices. I had in mind
// to not overthinking it so that source code would be easy to ready but also well written.

namespace CitiCsvParser.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new DependencyContainer();
            var argumentProvider = new ArgumentProvider(args);

            var arguments =
                new { argumentProvider.Path, argumentProvider.FilterColumn, argumentProvider.FilterValue };
            var csvImportService = container.Resolve<ICsvImportService>(arguments);

            csvImportService.ParseFile();
            csvImportService.ShowResult();
        }
    }
}
