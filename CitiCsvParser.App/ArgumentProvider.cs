﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CitiCsvParser.App
{
    public sealed class ArgumentProvider
    {
        private readonly List<string> _arguments;

        public ArgumentProvider(IEnumerable<string> arguments)
        {
            InitArgumentProperties();

            _arguments = arguments.ToList();
            if (_arguments.Any())
            {
                Path = _arguments[0];
            }

            SetFilterArguments();
        }

        public string Path { get; private set; }

        public string FilterColumn { get; private set; }

        public string FilterValue { get; private set; }

        private void InitArgumentProperties()
        {
            Path = String.Empty;
            FilterColumn = String.Empty;
            FilterValue = String.Empty;
        }

        private void SetFilterArguments()
        {
            bool isValue = false;
            for (int i = 1; i < _arguments.Count; i++)
            {
                if (_arguments[i].Equals("-value"))
                {
                    isValue = true;
                    continue;
                }

                if (!isValue)
                {
                    this.SetFilterColumn(_arguments[i]);
                }
                else
                {
                    this.SetFilterValue(_arguments[i]);
                }
            }
        }

        private void SetFilterColumn(string argument)
        {
            if (String.IsNullOrEmpty(this.FilterColumn))
            {
                this.FilterColumn = argument.Replace("-", "");
                return;
            }

            this.FilterColumn += " " + argument;
        }

        private void SetFilterValue(string argument)
        {
            if (String.IsNullOrEmpty(this.FilterValue))
            {
                this.FilterValue = argument;
                return;
            }

            this.FilterValue += " " + argument;
        }
    }
}
