﻿using System;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace CitiCsvParser.App.Dependencies
{
    public sealed class DependencyContainer : IDisposable
    {
        private readonly IWindsorContainer _container;

        public DependencyContainer()
        {
            _container = new WindsorContainer();

            _container.Register(
                Types.FromAssemblyNamed("CitiCsvParser.Bll")
                    .InNamespace("CitiCsvParser.Bll.Services")
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient());
        }

        public T Resolve<T>(object arguments)
        {
            return _container.Resolve<T>(arguments);
        }

        public void Dispose()
        {
            if (_container != null)
            {
                _container.Dispose();
            }
        }
    }
}
